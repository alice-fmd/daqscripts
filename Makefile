#
#
#
PACKAGE		= daqScripts_FMD
VERSION		= 0.1
RELEASE		= 1
distdir		= $(PACKAGE)-$(VERSION)
PREFIX		= /opt/$(subst _,-,$(PACKAGE))
FILES		= db/FMD_COMPUTE_THRESHOLDS		\
		  db/FMD_CONFIGURATION			\
		  db/FMD_COMPUTE_GAIN			\
		  db/SOR.commands			\
		  db/PAUSE.commands			\
		  bin/fmdPedestalDA.sh			\
		  bin/fmdReConf.sh			\
		  bin/fmdDbUpdate.sh			\
		  bin/fmdmenu				\
		  bin/fmdGainDA.sh			\
		  runOptions/PEDESTAL			\
		  runOptions/DEFAULT			\
		  runOptions/GAIN			


%:%.in
	sed -e 's/@PACKAGE@/$(PACKAGE)/g' 	\
	    -e 's/@VERSION@/$(VERSION)/g' 	\
	    -e 's/@RELEASE@/$(RELEASE)/g' 	\
	    -e 's,@PREFIX@,$(PREFIX),g' 	\
	    < $< > $@ 


install:	$(FILES)
	mkdir -p $(DESTDIR)$(PREFIX)
	$(foreach f, $^, 				\
	  mkdir -p $(DESTDIR)$(PREFIX)/$(dir $(f)) ; 	\
	  cp -a $(f) $(DESTDIR)$(PREFIX)/$(f) ;)

clean:
	rm -rf $(PACKAGE).spec

distdir:$(FILES) Makefile $(PACKAGE).spec $(PACKAGE).spec.in
	mkdir -p $(distdir)
	$(foreach f, $^, 				\
	  mkdir -p $(distdir)/$(dir $(f)) ; 		\
	  cp -a $(f) $(distdir)/$(f) ;)

dist:distdir
	tar -czvf $(distdir).tar.gz $(distdir)
	rm -rf $(distdir)


rpm-nosu:dist
	rpmbuild -D "_topdir $(HOME)/rpmbuild" -ta $(distdir).tar.gz

rpm:	dist
	sudo rpmbuild $(RPMFLAGS) -ta $(distdir).tar.gz
