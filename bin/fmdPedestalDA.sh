#!/bin/bash
# set -x 
# This script is severly hacked by Christian 

# Upload files to FXS if not done already 
uploadFile()
{
    local f=$1 
    local i=$2
    local r=$3  
    local c=`printf "%s_%09d.csv %s.csv" "${f}" "${r}" "${f}"` 
    for j in ${c} ; do 
	echo "Check if ${j} exists"
	if test -f ${j} ; then 
	    if test -f ${j}.upload ; then 
		echo "${j} already uploaded"
		return 0
	    fi 
	    
	    echo "Executing ${DAQDALIB_PATH}/daqFES_store ${j} ${i}"
	    ${DAQDALIB_PATH}/daqFES_store ${j} ${i}
	    ret=$? 
	    if test $ret -ne 0 ; then 
		echo "ERROR: upload failed!"
	    else 
		touch ${j}.upload 
	    fi
	    return $ret
	fi
    done 
    return 1
}	

# Some settings     
DADIR=/opt/daqDA-FMD-Pedestal
DAARGS="-Z"

# Debug settings 
# DADIR=/tmp
# DAARGS="-F -Z"
# export DAQDALIB_PATH=/tmp

# Identity on AMORE 
export AMORE_DA_NAME=FMD

# Go to working directory and clean up 
cd $DATE_SITE_WORKING_DIR 
rm -f peds.csv.upload 
rm -f conditions.csv.upload  

# Execute DA
${DADIR}/FMDPedestalda.exe ${DAARGS} $@ 
ret=$?

# In case of old faulty DA, reset return 
if test $ret -eq 2 && \
    test ! -f peds.csv.upload && 
    test ! -f conditions.csv.upload ; then 
    ret=0 
fi 

# Upload files if not done already 
uploadFile peds pedestals ${DATE_RUN_NUMBER}
uploadFile conditions conditions ${DATE_RUN_NUMBER} 

# Return to caller  
exit $ret

#
# EOF
#



