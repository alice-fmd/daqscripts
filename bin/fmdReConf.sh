#!/bin/bash 

# --- Type of reset to do on DDL -------------------------------------
resetType=hard 
doneTimeout=5
recoveryTimeout=3
useRecovery=0
reloadFec=0
resetRCU=0

# --- Define log file ------------------------------------------------
logdir=/dateSite/logFiles
if test ! -d $logdir && test ! -w $logdir ; then 
    logdir=/tmp
fi

# --- Wait for a specific state -------------------------------------
waitState()
{
    local srv=$1 ; shift 
    local tgt=$1 ; shift 
    local max=$1 ; shift 
    local cnt=0  ; shift 
    while test $cnt -lt $max ; do 
	sta=`/opt/dim/linux/dim_get_service ${srv}/STATE | grep ^H`
	sta=`echo "$sta" | sed -e "s/.*'\([a-zA-Z]*\).*'/\1/"`
	sta=`echo "$sta" | tr '[a-z]' '[A-Z]'`
	if test x${sta} = x || test x${sta} = xERROR  ; then 
	    # nothing returned or error state - bad 
	    return 1 
	fi
	if test x${sta} = x${tgt} ; then 
            # The state we're looking for 
            return 0
	fi

	# Not error, and not what we're waiting for 
	cnt=$((${cnt}+1))
	# Wait a while 
	sleep 1
    done 
    # If we get here, we have timed out 
    return 1
}

# --- Do one FEC -----------------------------------------------------
doOne()
{
    local url=$1  ; shift  
    local det=$1  ; shift 
    local ped=PEDCONF_FMD-FEE_0_0_${det}

    if test $useRecovery -gt 0 ; then 
	# If recovery state is requested, send Pedconf into the 
	# recovery state 
	/opt/dim/linux/dim_send_command ${ped}/GO "recover"

	# Wait for pedconf to change state 
	waitState "${ped}" "RECOVERY" ${recoveryTimeout}
	if test $? -gt 0; then 
	    # PedConf never changed state for some reason - fail
	    return 1
	fi
    fi
    echo "Will use URL=$url for FMD$det"

    local scr=`mktemp fmdReConf-FMD${det}-XXXXXX` 
    cat <<-EOF > $scr 
	echo "Storing some TTC registers"
	push TTCControl
	push ROIConfig1
	push POIConfig2
	push L1Timeout
	push L2Timeout
	push RoiTimeout
	push L1MsgtTmeout	
	push ALTROIF
	push ALTROCFG1
	push ALTROCFG2
	push TRGCONF
	EOF
    if test $resetRCU -gt 0 ; then 
	cat <<-EOF > $scr 
	echo "Reset RCU (RCU_RESET)"
	RCU_RESET 
	EOF
    fi 
    cat <<-EOF > $scr 
	echo "Align R/O clocks (ARM_ASYNCH)"
	ARM_ASYNCH
	echo "Reset trigger interface (TTCReset)"
	TTCReset 
	echo "Restore registers"
	pop
	EOF
    if test $reloadFec -gt 0 ; then 
	cat <<-EOF >> $scr 
	echo "Re-program BCs (CONFFEC)"
	CONFFEC 
	echo "Do 3 resets of FECs"
	sleep 2
	FECRST 
	sleep 1 
	FECRST 
	sleep 1 
	FECRST
	sleep 1
	EOF
    fi
    cat <<-EOF >> $scr 
	echo "Execute instructions (EXEC)"
	EXEC 
	echo "End of SOR procedure"
	exit 
	EOF
    

    cat $scr | rcuxx -f ${url}?reset=${resetType} 
    ret=$? 
    echo -e "SOR procedure of FMD${det} "
    if test $ret -ne 0 ; then 
	echo "failed"
	# This will put pedconf in the error state
	if test $useRecovery -gt 0 ; then 
	    /opt/dim/linux/dim_send_command ${ped}/GO "fail"
	fi
	return 1
    else 
	# To be implemented - this will put pedconf in the done state 
	if test $useRecovery -gt 0 ; then 
	    /opt/dim/linux/dim_send_command ${ped}/GO "success"
	fi
	echo "succeeded"
    fi

    if test $useRecovery -gt 0 ; then 
	# If recovery state is requested, wait for pedconf to change state 
	waitState "${ped}" "IDLE" $doneTimeout
	if test $? -gt 0; then 
	    # PedConf never changed state for some reason - fail
	    return 1
	fi
    fi

    return 0
}

# --- Help page ------------------------------------------------------
usage() 
{
    cat <<EOF
Usage: $0 [OPTIONS]

Options:
	--hard-reset			Do a hard reset 
	--soft-reset			Do a soft reset 
	--done-timeout SECONDS		How long to wait for DONE 
	--recovery-timeout SECONDS	How long to wait for RECOVERY
	--use-recovery			Use PedConf to signal recovery
	--no-recovery			Do not use PedConf 
	--reload-fec			Also reload FEC BC 
	--noload-fec			Do not reload FEC BC
	--reset-rcu                     Do reset the RCU 
	--no-reset-rcu                  Do not reset RCU[*]

[*] Resetting the RCU means resetting the DDL link - we shouldn't do that. 

EOF
}

while test $# -gt 0 ; do 
    case $1 in 
	--hard-reset)       resetType=hard ;; 
	--soft-reset)       resetType=soft ;; 
	--done-timeout)     doneTimeout=$2 ; shift ;; 
	--recovery-timeout) recoveryTimeout=$2 ; shift ;; 
	--use-recovery)     useRecovery=1 ; shift ;;
	--no-recovery)      useRecovery=0 ; shift ;; 
	--reload-fec)       reloadFec=1 ; shift ;;
	--noload-fec)       reloadFec=0 ; shift ;;
	--reset-rcu)        resetRCU=1 ; shift ;;
	--no-reset-rcu)     resetRCU=0 ; shift ;; 
	--help)             usage ; exit 0 ;; 
	*) echo "Unknown option: $1, ignored" ;; 
    esac
    shift 
done 

# --- Get list of FEEs -----------------------------------------------
fees=`/usr/bin/pedconf_getfees` 

# --- Loop over the list of FECs -------------------------------------
for c in $fees ; do 
    url=`echo $c | sed 's/.*@//'`	
    det=`echo $c | sed 's/FMD-FEE_0_0_\([0-9]\)@.*/\1/'`
    
    doOne "$url" "$det" $useRecovery > $logdir/FMD${det}.SOR.log
    ret=$?
    if test $ret -ne 0 ; then 
	echo "Failed" 
	break
    fi
done 

exit $ret
#
# EOF
#

    
