#!/bin/bash
#


# --- Function to upload ---------------------------------------------
upload() {
    local dbpath=$1 
    local ldir=$2 
    local input=`basename $dbpath` 


    if test "X$ldir" = "X" ; then ldir=. ; fi 
    if test ! -d $ldir  ; then 
	echo "$ldir is not a directory"
	exit 1
    fi
    if test ! -f ${ldir}/${input} ; then 
	echo "${ldir}/${input} does not exist"
	exit 1
    fi
    
    /date/db/copyFileDB.tcl set -f ${dbpath} -d ${ldir}
}
upload_det() {
    local dbpath=$1 
    local ldir=$2 
    local input=`basename $dbpath` 


    if test "X$ldir" = "X" ; then ldir=. ; fi 
    if test ! -d $ldir  ; then 
	echo "$ldir is not a directory"
	exit 1
    fi
    if test ! -f ${ldir}/${input} ; then 
	echo "${ldir}/${input} does not exist"
	exit 1
    fi
    /date/db/daqDetDB_store ${dbpath} ${ldir}
}

# --- The main stuff
prefix=/opt/daqScripts-FMD/
upload /ECS/FMD/FMD_COMPUTE_GAIN 	${prefix}/db
upload /ECS/FMD/FMD_COMPUTE_THRESHOLDS 	${prefix}/db
upload /ECS/FMD/FMD_CONFIGURATION 	${prefix}/db
upload /runControl/ropt/FMD/DEFAULT 	${prefix}/runOptions
upload /runControl/ropt/FMD/GAIN 	${prefix}/runOptions
upload /runControl/ropt/FMD/PEDESTAL 	${prefix}/runOptions
upload_det SOR.commands 		${prefix}/db
upload_det PAUSE.commands 		${prefix}/db


# EOF


